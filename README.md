# Yukk Full Stack Test Abdurrahman Saleh

## Intro

Hai tim YUKK! Nama saya Abdurrahman Saleh, biasa dipanggil Abi. Dan dengan readme.md ini, saya ingin menunjukkan hasil test interview yang sudah diberikan ke saya beberapa hari yang lalu untuk diselesaikan menggunakan **Laravel PHP** dan React JS.

## Done

### Backend

Dalam beberapa hari ini, saya telah menyisihkan sebagian waktu saya untuk mempelajari PHP lagi dan memahami penggunaan Laravel, dan saya telah berhasil membuat endpoint untuk

- Login
- Logout
- Register user
- Menampilkan detail rekening
- Menampilkan daftar transaksi
- Menampilkan detail rekening nasabah tujuan transfer
- Melakukan transfer

### Frontend

Adapun halaman yang sudah dibikin di react app saya adalah

- Halaman login
- Halaman register user
- Halaman menu utama
- Halaman detail rekening
- Halaman mutasi rekening / daftar transaksi
- Halaman pembuatan transaksi

## Not Done Yet

Berhubung saya belakangan ini juga sibuk dalam pekerjaan saya serta aktivitas saya yang banyak di hari libur, saya belum dapat membuat beberapa hal yang mungkin diminta juga di google drive requirement nya

### Backend

- Top up : saya masih belum paham untuk sistem top up ini harus seperti saya, saya hanya membuat relasi rekening dan transaksi sederhana (dengan asumsi saldo di dalamanya sudah ada)
- Error handling : ini juga hal yang masih saya kesulitan untuk lakukan di laravel secara tepat dengan tenggat waktu belajar laravel yang singkat. Saya sudah paham soal skema validasi, namun untuk menampilkan error dan kodenya, saya akan lanjut belajar lagi lebih lanjut.

### Frontend

- Pagination : transaksi perlu difilter dengan detail atau ID transaksi, karena keterbatasan waktu saya belum dapat mengimplementasi ini
- Formatting tanggal : daftar transaksi akan menampilkan waktu pembuatan transaksi, dan saya belum sempat juga mengubah tanggal tersebut sebagai tanggal yang _readable_

Meskipun itu, saya tidak ada keraguan untuk dapat menyelesaikan hal-hal yang kurang ini jikalau diberikan waktu lebih banyak lagi.

## Cara Penggunaan

### Setting Backend

1. Install laravel (saya mengikuti guide [ini](https://kinsta.com/knowledgebase/install-laravel/))
2. Install MYSQL jika belum
3. Jalankan `mysql -u root -p {NAMA_DB} < yukk_db.sql` jika ingin dapat DB yang sudah saya gunakan sebelumnya
4. Dari repository awal, `cd yukk_api`
5. `php artisan migrate`
6. Pastikan di `.env` nya ada config ini

```
SANCTUM_STATEFUL_DOMAINS=http://localhost:3000 // atau berapapun itu port frontend nya
SESSION_DOMAIN=localhost
```

7. `php artisan serve`

### Setting Frontend

Karena kesalahan ketika pembuatan branch, terpaksa saya hanya bisa memberikan file compress react-app saya

1. Unzip `yukk_ui.zip`
2. `cd yukk_ui`
3. `npm install` (jaga-jaga ternyata node_modules nya gak kebaca)
4. Pastikan di `.env` nya ada config ini

```
REACT_APP_API_BASE_URL='http://127.0.0.1:8000'
```

5. `npm run start`

Kabar baiknya adalah, semua node modules dan settingan lainnya sudah ada di dalam situ. Jadi _in theory_ harusnya setelah langkah di atas dilakukan, bisa langsung jalan. Tapi jika tidak, boleh contact saya lagi lewat email **abi_abdurrahman@hotmail.com**
