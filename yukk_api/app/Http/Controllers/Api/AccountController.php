<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Managers\Api\Account\ShowManager;
use App\Managers\Api\Account\ShowRecipientManager;
use Illuminate\Http\Request;

class AccountController extends Controller {

    protected $showRecipientManager;
    protected $showManager;

    public function __construct(
        ShowRecipientManager $showRecipientManager,
        ShowManager $showManager
    ) {
        $this->showRecipientManager = $showRecipientManager;
        $this->showManager = $showManager;
    }

    public function show(Request $request) {
        $account = $this->showManager->execute($request);

        return response()->json([
            'success' => true,
            'account' => $account
        ]);
    }

    public function show_recipient(Request $request) {
        $account = $this->showRecipientManager->execute($request);

        return response()->json([
            'success' => true,
            'account' => $account
        ]);
    }
}
