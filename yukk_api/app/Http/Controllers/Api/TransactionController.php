<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Managers\Api\Transaction\CreateManager;
use App\Managers\Api\Transaction\IndexManager;
use App\Managers\Api\Transaction\ShowManager;
use Illuminate\Http\Request;

class TransactionController extends Controller {

    protected $createManager;
    protected $indexManager;
    protected $showManager;

    public function __construct(
        CreateManager $createManager,
        IndexManager $indexManager,
        ShowManager $showManager
    ) {
        $this->createManager = $createManager;
        $this->indexManager = $indexManager;
        $this->showManager = $showManager;
    }

    public function index(Request $request) {
        $transactions = $this->indexManager->execute($request);

        return response()->json($transactions);
    }

    public function show(string $id) {
        $transaction = $this->showManager->execute($id);

        return response()->json($transaction);
     }

    public function store(Request $request) {
        $transaction = $this->createManager->execute($request);

        //return response JSON user is created
        if($transaction) {
            return response()->json([
                'success' => true,
                'transaction' => $transaction,
            ], 201);
        }

        //return JSON process insert failed
        return response()->json([
            'success' => false,
        ], 409);
    }
}
