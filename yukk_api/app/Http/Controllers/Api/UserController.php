<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Managers\Api\User\CreateManager;
use App\Managers\Api\User\ShowManager;
use Illuminate\Http\Request;

class UserController extends Controller {

    protected $createManager;
    protected $showManager;

    public function __construct(
        CreateManager $createManager,
        ShowManager $showManager
    ) {
        $this->createManager = $createManager;
        $this->showManager = $showManager;
    }

    public function show(Request $request) {
        $user = $this->showManager->execute($request);

        return response()->json([
            'success' => true,
            'user'    => $user,
        ]);
    }

    public function store(Request $request) {

        $user = $this->createManager->execute($request);

        //return response JSON user is created
        if($user) {
            return response()->json([
                'success' => true,
                'user'    => $user,
            ], 201);
        }

        //return JSON process insert failed
        return response()->json([
            'success' => false,
        ], 409);

        // return new InvoiceResource(true, 'Pembuatan Invoice Berhasil!', $invoice);
    }
}
