<?php

namespace App\Managers\Api\Account;
use App\Models\Account;
use App\Models\User;
use Illuminate\Http\Request;

class ShowManager {
    public function execute(Request $request) {
        $id = $request->user()->id;

        $account = Account::find($id);

        if ($account == null) return response()->json("Account Not Found", 404);

        return $account;
    }
}
