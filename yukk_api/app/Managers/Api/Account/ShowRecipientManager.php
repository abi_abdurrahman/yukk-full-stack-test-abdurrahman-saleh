<?php

namespace App\Managers\Api\Account;
use App\Models\Account;
use App\Models\User;
use Illuminate\Http\Request;

class ShowRecipientManager {
    public function execute(Request $request) {
        $number = $request->input('number');

        error_log($request->getContent());
        $account = Account::where('number', $number)->first();

        if ($account == null) return response()->json("Account Not Found", 404);

        $account = [
            'id' => $account->id,
            'number' => $account->number,
            'recipient_name' => $account->user->name
        ];

        return $account;
    }
}
