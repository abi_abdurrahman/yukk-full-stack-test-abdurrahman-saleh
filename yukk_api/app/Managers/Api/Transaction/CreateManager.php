<?php

namespace App\Managers\Api\Transaction;
use App\Models\Account;
use App\Models\Transaction;
use App\Models\User;
use App\Rules\ValidateTransactionAmount;
use Illuminate\Http\Request;
use Validator;

use Illuminate\Support\Facades\DB;

class CreateManager {
    public function execute(Request $request) {
        $validator = Validator::make($request->all(), [
            'amount' => ['required', new ValidateTransactionAmount($request)],
            'recipient_account_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        DB::beginTransaction();

        try {
            //code...
            $sender_account = Account::where("user_id" , $request->user()->id)->first();
            $recipient_account = Account::find($request->input("recipient_account_id"));
            $amount = $request->input('amount');
            $new_sender_balance = $sender_account->balance - $amount;
            $new_recipient_balance = $recipient_account->balance + $amount;


            $recipient_account->balance = $new_recipient_balance;
            $sender_account->balance = $new_sender_balance;

            // save the account changes 1st
            $sender_account->save();
            $recipient_account->save();

            // create the transaction
            $transaction = Transaction::create([
                'recipient_account_id' => $recipient_account->id,
                'sender_account_id' => $sender_account->id,
                'amount' => $amount,
                'detail' => $request->input('detail')
            ]);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json($th, 404);
        } catch (\Exception $th) {
            DB::rollBack();
            return response()->json($th, 404);
        }

        DB::commit();

        return [
            'id' => $transaction->id,
            'amount' => $transaction->amount,
            'recipient_name' => $recipient_account->user->name,
            'recipient_account_number' => $recipient_account->number,
            'sender_account_number' => $sender_account->number
        ];
    }
}
