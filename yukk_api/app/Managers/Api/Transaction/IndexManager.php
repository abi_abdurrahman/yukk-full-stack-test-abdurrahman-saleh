<?php

namespace App\Managers\Api\Transaction;
use App\Helpers\PaginationHelper;
use App\Models\Transaction;
use Illuminate\Http\Request;

class IndexManager {
    public function execute(Request $request) {
        $per_page = $request->input("per_page",10);
        $user = $request->user();
        $account = $user->account;

        $transactions = Transaction::where('recipient_account_id', $account->id)
                                   ->orWhere('sender_account_id', $account->id)
                                   ->get();

        $transactions = PaginationHelper::paginate($transactions, $per_page);

        return $transactions;
    }
}
