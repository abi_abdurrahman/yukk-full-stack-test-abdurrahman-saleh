<?php

namespace App\Managers\Api\Transaction;
use App\Models\Account;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Request;

class ShowManager {
    public function execute(string $id) {

        $transaction = Transaction::find($id);

        if ($transaction == null) return response()->json("Transaction Not Found", 404);

        return [
            'id' => $transaction->id,
            'amount' => $transaction->amount,
            'detail' => $transaction->detail,
            'created_at' => $transaction->created_at,
            'recipient_name' => $transaction->recipient_account->user->name,
            'recipient_account_number' => $transaction->sender_account->number,
            'sender_account_number' => $transaction->sender_account->number
        ];
    }
}
