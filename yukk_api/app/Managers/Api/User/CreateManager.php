<?php

namespace App\Managers\Api\User;
use App\Models\Account;
use App\Models\User;
use Illuminate\Http\Request;
use Validator;

use Illuminate\Support\Facades\DB;

class CreateManager {
    public function execute(Request $request) {
        $validator = Validator::make($request->all(), [
            'name'      => 'required',
            'email'     => 'required|email|unique:users',
            'password'  => 'required|min:8|confirmed'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //create user
        $user = User::create([
            'name'      => $request->name,
            'email'     => $request->email,
            'password'  => bcrypt($request->password)
        ]);

        $existing_accounts = Account::all();

        $account_numbers = $existing_accounts->pluck('number')->toArray();

        $generated_acc_number = mt_rand(1000000000,9999999999);

        while (in_array($generated_acc_number, $account_numbers)) {
            $generated_acc_number = mt_rand(1000000000,9999999999);
        }

        Account::create([
            'user_id' => $user->id,
            'number' => $generated_acc_number
        ]);

        return $user;
    }
}
