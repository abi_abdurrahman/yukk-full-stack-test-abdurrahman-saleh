<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Transaction extends Model
{
    use HasFactory;

    protected $fillable = ['amount', 'detail', 'recipient_account_id', 'sender_account_id'];

    public function recipient_account(): BelongsTo {
        return $this->belongsTo(Account::class);
    }

    public function sender_account(): BelongsTo {
        return $this->belongsTo(Account::class);
    }
}
