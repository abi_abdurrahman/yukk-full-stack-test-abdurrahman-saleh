<?php

namespace App\Rules;

use App\Models\Account;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Contracts\Validation\ValidationRule;

class ValidateTransactionAmount implements ValidationRule
{
    protected $request;

    public function __construct(Request $request) {
        $this->request = $request;
    }

    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $user_id = $this->request->user()->id;

        $sender_account = Account::where("user_id", $user_id)->first();

        if ($sender_account->balance < $value) {
            $fail('The amount has exceeded the existing balance');
        }
    }
}
