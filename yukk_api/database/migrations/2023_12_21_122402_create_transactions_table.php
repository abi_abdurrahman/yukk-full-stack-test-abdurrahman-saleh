<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('recipient_account_id');
            $table->unsignedBigInteger('sender_account_id');
            $table->foreign('recipient_account_id')->references('id')->on('accounts');
            $table->foreign('sender_account_id')->references('id')->on('accounts');
            $table->float('amount');
            $table->string('detail')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('transactions');
    }
};
