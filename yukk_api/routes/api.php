<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:sanctum')->group(function () {
    Route::post('/logout', [\App\Http\Controllers\Api\AuthController::class, 'logout']);
    Route::get('/account', [\App\Http\Controllers\Api\AccountController::class,'show']);
    Route::get('/recipient_account', [\App\Http\Controllers\Api\AccountController::class,'show_recipient']);
    Route::post('/transaction', [\App\Http\Controllers\Api\TransactionController::class,'store']);
    Route::get('/transaction', [App\Http\Controllers\Api\TransactionController::class, 'index']);
    Route::get('/transaction/{id}', [App\Http\Controllers\Api\TransactionController::class, 'show']);
});

Route::apiResource('/users', App\Http\Controllers\Api\UserController::class);

Route::post('/login', [\App\Http\Controllers\Api\AuthController::class, 'login']);
